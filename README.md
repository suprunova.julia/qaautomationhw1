Implement test cases for following scenarios:
1. Create an item without a category
2. Create an item with a category
3. Validate itemDescription field requirements
4. Validate itemName field requirements
5. Validate price field requirements
6. Validate quantity field requirements
7. Create an item and get it
8. Create an item and update it
9. Create an item and delete it
10. Get all items
11. Complete registration and login
12. Create a comment for item
13. Validate comment field requirements
14. Validate itemId field requirements
15. Validate stars field requirements
16. Update comment
17. Get all user comments
18. Delete comment
19. Get comment by id
20. Get all comments for an item
21. Update new order as admin
- Create order using /api/user/orderFast endpoint
- Wait for the order to receive the PROCESSING status
- Update order by admin
22. Create new comment and wait util it appears in monitoring service
