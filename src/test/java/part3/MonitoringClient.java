package part3;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.SneakyThrows;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import part2.*;

import java.util.List;

public class MonitoringClient extends BaseClient {
  @SneakyThrows
  public ResponseWrapper<List<OrderDTO>> getItems(){
    HttpGet httpGet = new HttpGet(BASE_PATH + "/monitoring/api/orders");
    CloseableHttpResponse httpResponse = client.execute(httpGet);
    return new ResponseWrapper<>(httpResponse, new TypeReference<List<OrderDTO>>() {
    });
  }

  @SneakyThrows
  public ResponseWrapper<List<CommentDTO>> getMonitoringComments(UserDTO userDto){
    HttpGet httpGet = new HttpGet(BASE_PATH + "/monitoring/api/comments?userId="+userDto.getId());
    CloseableHttpResponse httpResponse = client.execute(httpGet);
    return new ResponseWrapper<>(httpResponse, new TypeReference<List<CommentDTO>>() {
    });
  }
}
