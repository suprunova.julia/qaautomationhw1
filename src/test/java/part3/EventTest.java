package part3;

import com.beust.jcommander.internal.Lists;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import part2.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class EventTest {
  private final UserClient userClient = new UserClient();
  private final PublicClient publicClient = new PublicClient();
  private UserDTO user = null;
  private final String username = "Username" + System.currentTimeMillis();
  private final String pass = "Password" + System.currentTimeMillis();

  @BeforeClass
  public void registerClient() {
    RegistrationDTO registrationDto = RegistrationDTO
        .builder()
        .username(username)
        .password(pass)
        .passwordConfirm(pass)
        .build();
    user = userClient
        .register(registrationDto)
        .statusCodeIs(200)
        .parseContent();
    userClient.login(username, pass);
  }

  @Test
  public void createSlowOrder() {
    ShopItemDTO shopItemDTO = publicClient
        .getItems().statusCodeIs(200)
        .parseContent()
        .stream()
        .findAny()
        .orElseThrow(() -> new AssertionError("Cannot find any items"));

    OrderDTO orderDTO = OrderDTO
        .builder()
        .items(Lists.newArrayList(
            ItemQuantityDTO
                .builder()
                .itemId(shopItemDTO.getId())
                .quantity(1L)
                .build()))
        .build();
    long maxWait = 5000L;
    long before = System.currentTimeMillis();
    OrderDTO orderResponseDto = userClient
        .createOrderSlow(orderDTO)
        .statusCodeIs(200)
        .parseContent();
    long after = System.currentTimeMillis();

    Assert.assertEquals(orderResponseDto.getStatus(), "PROCESSING");
    Assert.assertEquals(orderResponseDto.getItemReserved(), Boolean.TRUE);
    Assert.assertEquals(orderResponseDto.getNotificationSend(), Boolean.TRUE);
    Assert.assertTrue(maxWait > after - before, "Max wait exceeded " + (after - before));
  }

  @Test
  public void createFastOrder() {
    ShopItemDTO shopItemDTO = publicClient
        .getItems()
        .statusCodeIs(200)
        .parseContent()
        .stream()
        .findAny()
        .orElseThrow(() -> new AssertionError("Cannot find any items"));

    OrderDTO orderDTO = OrderDTO
        .builder()
        .items(Lists.newArrayList(
            ItemQuantityDTO
                .builder()
                .itemId(shopItemDTO.getId())
                .quantity(1L)
                .build()))
        .build();

    long before = System.currentTimeMillis();
    OrderDTO orderResponseDTO = userClient
        .createOrderFast(orderDTO)
        .statusCodeIs(200)
        .parseContent();
    long orderId = orderResponseDTO.getId();
    long maxWait = 5000L;
    long after = System.currentTimeMillis();

    while (!orderResponseDTO.getStatus().equals("PROCESSING")) {
      after = System.currentTimeMillis();
      orderResponseDTO = userClient
          .getOrderById((int) orderId)
          .statusCodeIs(200)
          .parseContent();
    }
    Assert.assertEquals(orderResponseDTO.getItemReserved(), Boolean.TRUE);
    Assert.assertEquals(orderResponseDTO.getNotificationSend(), Boolean.TRUE);
    Assert.assertTrue(maxWait > after - before, "Max wait exceeded " + (after - before));
  }

  @Test
  public void getOrderList() throws InterruptedException {
    ShopItemDTO shopItemDTO = publicClient
        .getItems()
        .statusCodeIs(200)
        .parseContent()
        .stream()
        .findAny()
        .orElseThrow(() -> new AssertionError("Cannot find any items"));
    OrderDTO orderDTO = OrderDTO
        .builder()
        .items(Lists.newArrayList(
            ItemQuantityDTO
                .builder()
                .itemId(shopItemDTO.getId())
                .quantity(1L)
                .build()))
        .build();

    long before = System.currentTimeMillis();
    OrderDTO orderResponseDTO = userClient
        .createOrderFast(orderDTO)
        .statusCodeIs(200)
        .parseContent();

    long orderId = orderResponseDTO.getId();

    MonitoringClient monitoringClient = new MonitoringClient();
    List<OrderDTO> orders = monitoringClient.getItems().statusCodeIs(200).parseContent();
    long maxWait = 5000L;

    while (orders.stream().noneMatch(order -> order.getId().equals(orderId))) {
      orders = monitoringClient
          .getItems()
          .statusCodeIs(200)
          .parseContent();
      long after = System.currentTimeMillis();
      TimeUnit.MILLISECONDS.sleep(110L);
      Assert.assertTrue(maxWait > after - before, "Max wait exceeded " + (after - before));
    }
  }

  @Test(testName = "21. Update new order as admin")
  public void updateNewOrderAsAdmin() throws InterruptedException {

    ShopItemDTO shopItemDTO = publicClient
        .getItems()
        .statusCodeIs(200)
        .parseContent()
        .stream()
        .findAny()
        .orElseThrow(() -> new AssertionError("Cannot find any items"));
    OrderDTO orderDTO = OrderDTO
        .builder()
        .items(Lists.newArrayList(
            ItemQuantityDTO
                .builder()
                .itemId(shopItemDTO.getId())
                .quantity(1L)
                .build()))
        .build();
    OrderDTO orderResponseDTO = userClient
        .createOrderFast(orderDTO)
        .statusCodeIs(200)
        .parseContent();

    long orderId = orderResponseDTO.getId();
    long maxWait = 5000L;
    long before = System.currentTimeMillis();

    while (!orderResponseDTO.getStatus().equals("PROCESSING")) {
      orderResponseDTO = userClient
          .getOrderById((int) orderId)
          .statusCodeIs(200)
          .parseContent();

      long after = System.currentTimeMillis();
      TimeUnit.MILLISECONDS.sleep(110L);
      Assert.assertTrue(maxWait > after - before, "Max wait exceeded " + (after - before));
    }

    AdminClient adminClient = new AdminClient();
    orderResponseDTO.setStatus("RECEIVED");
    adminClient.updateOrder(orderResponseDTO);

    Assert.assertEquals(orderResponseDTO.getStatus(), "RECEIVED");
    Assert.assertEquals(orderResponseDTO.getItemReserved(), Boolean.TRUE);
    Assert.assertEquals(orderResponseDTO.getNotificationSend(), Boolean.TRUE);
  }

  @Test(testName = "22. Create new comment and wait util it appears in monitoring service")
  public void createCommentAndWait() throws InterruptedException {
    userClient.login(username, pass);

    ShopItemDTO shopItemDTO = ShopItemDTO.builder()
        .itemName("desc")
        .itemDescription("wooden desc")
        .price(160f)
        .quantity(10)
        .build();
    AdminClient adminClient = new AdminClient();
    ShopItemDTO item = adminClient.createItem(shopItemDTO).parseContent();

    CommentDTO comment = CommentDTO.builder()
        .comment("Super desc!")
        .stars(4)
        .itemId(item.getId())
        .build();
    CommentDTO responseComment = userClient
        .createComment(comment)
        .statusCodeIs(201)
        .parseContent();

    MonitoringClient monitoringClient = new MonitoringClient();
    List<CommentDTO> comments = monitoringClient
        .getMonitoringComments(user)
        .statusCodeIs(200)
        .parseContent();
    long maxWait = 5000L;
    long before = System.currentTimeMillis();

    while (comments.stream().noneMatch(comm -> comm.getId().equals(responseComment.getId()))) {
      long after = System.currentTimeMillis();
      TimeUnit.MILLISECONDS.sleep(110L);
      Assert.assertTrue(maxWait > after - before,
          "Max wait exceeded " + (after - before));
      comments = monitoringClient
          .getMonitoringComments(user)
          .statusCodeIs(200)
          .parseContent();
    }

  }

}
