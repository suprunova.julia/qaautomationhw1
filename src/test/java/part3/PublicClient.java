package part3;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.SneakyThrows;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import part2.BaseClient;
import part2.ResponseWrapper;
import part2.ShopItemDTO;

import java.util.List;

public class PublicClient extends BaseClient {
  @SneakyThrows
  public ResponseWrapper<List<ShopItemDTO>> getItems(){
    HttpGet httpGet = new HttpGet(BASE_PATH + "/api/shopItems");
    CloseableHttpResponse httpResponse = client.execute(httpGet);
    return new ResponseWrapper<>(httpResponse, new TypeReference<List<ShopItemDTO>>() {
    });
  }
}
