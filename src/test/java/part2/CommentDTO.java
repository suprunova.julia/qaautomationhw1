package part2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
//*"comment": "string", - not blank
//  "id": 0, - should be present in response
//  "itemId": 0, should be equal to path variable from post request
//  "stars": 0 - (1 <= stars <= 5)
public class CommentDTO {
  private String comment;
  private Long id;
  private Long itemId;
  private Integer stars;
}
