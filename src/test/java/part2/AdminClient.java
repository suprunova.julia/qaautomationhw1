package part2;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.SneakyThrows;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import part3.OrderDTO;

public class AdminClient extends BaseClient {
  @SneakyThrows
  public ResponseWrapper<ShopItemDTO> createItem(ShopItemDTO shopItemDTO) {
    HttpPost httpPost = new HttpPost(BASE_PATH + "/admin/api/v1/shopItem");
    httpPost.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
    httpPost.setEntity(new StringEntity(mapper.writeValueAsString(shopItemDTO)));

    CloseableHttpResponse httpResponse = client.execute(httpPost);
    return new ResponseWrapper<>(httpResponse, new TypeReference<ShopItemDTO>() {
    });
  }
  @SneakyThrows
  public ResponseWrapper<OrderDTO> updateOrder(OrderDTO orderDTO) {
    HttpPut httpPut = new HttpPut(BASE_PATH + "/admin/api/v2/order");
    httpPut.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
    httpPut.setEntity(new StringEntity(mapper.writeValueAsString(orderDTO)));

    CloseableHttpResponse httpResponse = client.execute(httpPut);
    return new ResponseWrapper<>(httpResponse, new TypeReference<OrderDTO>() {
    });
  }
}
