package part2;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public abstract class BaseClient {
  protected static final String BASE_PATH = "http://207.154.241.206:8080";
  protected CloseableHttpClient client = HttpClients.createDefault();
  protected ObjectMapper mapper = new ObjectMapper();
}
