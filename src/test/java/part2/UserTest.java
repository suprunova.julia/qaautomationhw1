package part2;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class UserTest {
  public static final String BASE_PATH = "http://207.154.241.206:8080";
  private final UserClient userClient = new UserClient();
  private final AdminClient adminClient = new AdminClient();
  private final String username = "Username" + System.currentTimeMillis();
  private final String pass = "Password" + System.currentTimeMillis();

  public ShopItemDTO createItem() {
    ShopItemDTO shopItemDTO = ShopItemDTO.builder()
        .itemName("desc")
        .itemDescription("wooden desc")
        .price(160f)
        .quantity(10)
        .build();
    return adminClient.createItem(shopItemDTO).parseContent();
  }

  @BeforeClass
  public void registerClient() {
    RegistrationDTO registrationDto = RegistrationDTO
        .builder()
        .username(username)
        .password(pass)
        .passwordConfirm(pass)
        .build();
    userClient
        .register(registrationDto)
        .statusCodeIs(200);
  }

  @Test(testName = "11. Complete registration and login(positive)")
  public void loginWithPass() {
    boolean loginResponse = userClient
        .login(username, pass)
        .statusCodeIs(302)
        .getHeaders()
        .toString()
        .contains(BASE_PATH + "/" + "login?error");
    Assert.assertFalse(loginResponse);
  }

  @Test(testName = "11. Complete registration and login(negative)")
  public void loginWithWrongPass() {
    boolean loginResponse = userClient
        .login(username, "8")
        .statusCodeIs(302)
        .getHeaders()
        .toString()
        .contains(BASE_PATH + "/" + "login?error");
    Assert.assertTrue(loginResponse);
  }

  @Test(testName = "12. Create a comment for item(positive)")
  public void creatingComment() {
    SoftAssert softAssert = new SoftAssert();
    userClient.login(username, pass);
    ShopItemDTO item = createItem();
    CommentDTO comment = CommentDTO.builder()
        .comment(".")
        .stars(4)
        .itemId(item.getId())
        .build();
    CommentDTO comment2 = userClient
        .createComment(comment)
        .statusCodeIs(201)
        .parseContent();
    softAssert.assertEquals(comment2.getComment(), comment.getComment());
  }

  @Test(testName = "12. Create a comment for item (negative)")
  public void createCommentWithoutLogin() {
    UserClient userClient = new UserClient();
    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("comment")
        .stars(4)
        .itemId(item.getId())
        .build();
    userClient
        .createComment(comment)
        .statusCodeIs(302);
  }

  @Test(testName = "13. Validate comment field requirements (positive)")
  public void validateComment() {
    SoftAssert softAssert = new SoftAssert();
    userClient.login(username, pass);
    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("Cool desk!")
        .stars(5)
        .itemId(item.getId())
        .build();
    CommentDTO comment2 = userClient
        .createComment(comment)
        .statusCodeIs(201)
        .parseContent();

    softAssert.assertFalse(comment2.getComment().isBlank());
    Assert.assertNotNull(comment2.getId());
  }

  @Test(testName = "13. Validate comment field requirements (negative)")
  public void commentWithBlankComment() {
    userClient.login(username, pass);
    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("")
        .stars(4)
        .itemId(item.getId())
        .build();
    userClient
        .createComment(comment)
        .statusCodeIs(400);
  }

  @Test(testName = "14. Validate itemId field requirements (positive)")
  public void validateItemId() {
    SoftAssert softAssert = new SoftAssert();
    userClient.login(username, pass);
    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("Cool desk!")
        .stars(5)
        .itemId(item.getId())
        .build();
    CommentDTO comment2 = userClient
        .createComment(comment)
        .statusCodeIs(201)
        .parseContent();
    softAssert.assertEquals(comment2.getItemId(), item.getId());
  }

  @Test(testName = "14. Validate itemId field requirements (negative)")
  public void validateNullItemId() {
    userClient.login(username, pass);
    CommentDTO comment = CommentDTO.builder()
        .comment("Cool desk!")
        .stars(5)
        .build();
    userClient
        .createComment(comment)
        .statusCodeIs(400);
  }

  @Test(testName = "15. Validate stars field requirements (positive)")
  public void validate1Star() {
    userClient.login(username, pass);
    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("Cool desk!")
        .stars(1)
        .itemId(item.getId())
        .build();
    userClient
        .createComment(comment)
        .statusCodeIs(201);
  }

  @Test(testName = "15. Validate stars field requirements (positive)")
  public void validate5Stars() {
    userClient.login(username, pass);
    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("Cool desk!")
        .stars(5)
        .itemId(item.getId())
        .build();
    userClient
        .createComment(comment)
        .statusCodeIs(201);
  }

  @Test(testName = "15. Validate stars field requirements (negative)")
  public void validate0Stars() {
    userClient.login(username, pass);
    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("Cool desk!")
        .stars(0)
        .itemId(item.getId())
        .build();
    userClient
        .createComment(comment)
        .statusCodeIs(400);
  }

  @Test(testName = "15. Validate stars field requirements (negative)")
  public void validate6Stars() {
    userClient.login(username, pass);
    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("Cool desk!")
        .stars(6)
        .itemId(item.getId())
        .build();
    userClient
        .createComment(comment)
        .statusCodeIs(400);
  }

  @Test(testName = "16. Update comment (positive)")
  public void updateComment() {
    userClient.login(username, pass);
    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("Cool desk!")
        .stars(5)
        .itemId(item.getId())
        .build();
    CommentDTO comment2 = userClient
        .createComment(comment)
        .statusCodeIs(201)
        .parseContent();
    comment2.setComment("Bad desk");
    CommentDTO newComment = userClient
        .updateComment(comment2)
        .statusCodeIs(200)
        .parseContent();
    Assert.assertEquals(newComment.getComment(), comment2.getComment());
  }

  @Test(testName = "17. Get all user comments(positive)")
  public void getUserCommentsAboutOneItem() {
    SoftAssert softAssert = new SoftAssert();
    userClient.login(username, pass);
    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("good desc")
        .stars(4)
        .itemId(item.getId())
        .build();
    CommentDTO comment2 = userClient
        .createComment(comment)
        .statusCodeIs(201)
        .parseContent();
    softAssert.assertEquals(comment2.getComment(), comment.getComment());

    CommentDTO newComment = CommentDTO.builder()
        .comment("perfect desc")
        .stars(5)
        .itemId(item.getId())
        .build();
    CommentDTO newComment2 = userClient
        .createComment(newComment)
        .statusCodeIs(201)
        .parseContent();
    softAssert.assertEquals(newComment2.getComment(), newComment.getComment());

    List<CommentDTO> comments = userClient.getAllComments().statusCodeIs(200).parseContent();
    softAssert.assertEquals(2, comments.size());
    softAssert.assertEquals(comment2, comments.get(0));
    softAssert.assertEquals(newComment2, comments.get(1));
  }

  @Test(testName = "17. Get all user comments (positive)")
  public void getUserCommentsAboutTwoItems() {
    SoftAssert softAssert = new SoftAssert();
    userClient.login(username, pass);

    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("good desc")
        .stars(4)
        .itemId(item.getId())
        .build();
    CommentDTO comment2 = userClient
        .createComment(comment)
        .statusCodeIs(201)
        .parseContent();
    softAssert.assertEquals(comment2.getComment(), comment.getComment());

    ShopItemDTO chairDTO = ShopItemDTO.builder()
        .itemName("chair")
        .itemDescription("plastic chair")
        .price(100f)
        .quantity(0)
        .build();
    ShopItemDTO newItem = adminClient.createItem(chairDTO).parseContent();

    CommentDTO newComment = CommentDTO.builder()
        .comment("perfect chair")
        .stars(5)
        .itemId(newItem.getId())
        .build();
    CommentDTO newComment2 = userClient
        .createComment(newComment)
        .statusCodeIs(201)
        .parseContent();
    softAssert.assertEquals(newComment2.getComment(), newComment.getComment());

    List<CommentDTO> comments = userClient.getAllComments().statusCodeIs(200).parseContent();
    softAssert.assertEquals(2, comments.size());
    softAssert.assertEquals(comment2, comments.get(0));
    softAssert.assertEquals(newComment2, comments.get(1));
  }

  @Test(testName = "18. Delete comment (positive)")
  public void deleteComment() {
    SoftAssert softAssert = new SoftAssert();
    userClient.login(username, pass);

    ShopItemDTO item = createItem();
    CommentDTO comment = CommentDTO.builder()
        .comment("cool item")
        .stars(4)
        .itemId(item.getId())
        .build();
    CommentDTO comment2 = userClient
        .createComment(comment)
        .statusCodeIs(201)
        .parseContent();
    softAssert.assertEquals(comment2.getComment(), comment.getComment());

    userClient.deleteComment(comment2).statusCodeIs(204);
    Assert.assertEquals(userClient.getAllComments().parseContent().size(), 0);
  }

  @Test(testName = "19. Get comment by id (positive)")
  public void getCommentById() {
    userClient.login(username, pass);

    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("cool item")
        .stars(4)
        .itemId(item.getId())
        .build();
    CommentDTO comment2 = userClient
        .createComment(comment)
        .statusCodeIs(201)
        .parseContent();

    Assert.assertEquals(comment2, userClient
        .getComment(comment2.getId())
        .statusCodeIs(200)
        .parseContent());
  }

  @Test(testName = "20. Get all comments for an item (positive)")
  public void getCommentsForItem() {
    userClient.login(username, pass);
    ShopItemDTO item = createItem();

    CommentDTO comment = CommentDTO.builder()
        .comment("cool item")
        .stars(4)
        .itemId(item.getId())
        .build();
    CommentDTO comment2 = userClient
        .createComment(comment)
        .statusCodeIs(201)
        .parseContent();
    // new user
    String username = "Username" + System.currentTimeMillis();
    String pass = "Password" + System.currentTimeMillis();
    UserClient userClient = new UserClient();
    RegistrationDTO registrationDto = RegistrationDTO.builder()
        .username(username)
        .password(pass)
        .passwordConfirm(pass)
        .build();
    userClient
        .register(registrationDto)
        .statusCodeIs(200);
    userClient.login(username, pass);

    CommentDTO comment3 = CommentDTO.builder()
        .comment("hot item")
        .stars(5)
        .itemId(item.getId())
        .build();
    CommentDTO comment4 = userClient
        .createComment(comment3)
        .statusCodeIs(201)
        .parseContent();

    List<CommentDTO> comments = userClient
        .getItemComments(item.getId())
        .statusCodeIs(200)
        .parseContent();
    Assert.assertEquals(comments.size(), 2);
    Assert.assertEquals(comments.get(0), comment2);
    Assert.assertEquals(comments.get(1), comment4);
  }
}