package part2;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.SneakyThrows;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.testng.collections.Lists;
import part3.OrderDTO;

import java.util.List;


public class UserClient extends BaseClient {
  @SneakyThrows
  public ResponseWrapper<UserDTO> register(RegistrationDTO registrationDto) {
    HttpPost httpPost = new HttpPost(BASE_PATH + "/api/registration");
    httpPost.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
    httpPost.setEntity(new StringEntity(mapper.writeValueAsString(registrationDto)));

    CloseableHttpResponse httpResponse = client.execute(httpPost);
    return new ResponseWrapper<>(httpResponse, new TypeReference<UserDTO>() {
    });
  }

  @SneakyThrows
  public ResponseWrapper<String> login(String login, String password) {
    List<NameValuePair> pairs = Lists.newArrayList(new BasicNameValuePair("username", login),
        new BasicNameValuePair("password", password));

    HttpPost httpPost = new HttpPost(BASE_PATH + "/login");
    httpPost.setEntity(new UrlEncodedFormEntity(pairs));

    CloseableHttpResponse httpResponse = client.execute(httpPost);
    return new ResponseWrapper<>(httpResponse, new TypeReference<String>() {
    });
  }

  @SneakyThrows
  public ResponseWrapper<UserInfoDTO> getUserInfo() {
    HttpGet httpGet = new HttpGet(BASE_PATH + "/api/user/info");
    CloseableHttpResponse httpResponse = client.execute(httpGet);
    return new ResponseWrapper<>(httpResponse, new TypeReference<UserInfoDTO>() {
    });
  }

  @SneakyThrows
  public ResponseWrapper<UserInfoDTO> updateUserInfo(UserInfoDTO userInfoDto) {
    HttpPut httpPut = new HttpPut(BASE_PATH + "/api/user/info");
    httpPut.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
    httpPut.setEntity(new StringEntity(mapper.writeValueAsString(userInfoDto)));

    CloseableHttpResponse httpResponse = client.execute(httpPut);

    return new ResponseWrapper<>(httpResponse, new TypeReference<UserInfoDTO>() {
    });
  }

  @SneakyThrows
  public ResponseWrapper<CommentDTO> createComment(CommentDTO commentDTO) {
    HttpPost httpPost = new HttpPost(BASE_PATH + "/api/user/item/" + commentDTO.getItemId() + "/comment");
    httpPost.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
    httpPost.setEntity(new StringEntity(mapper.writeValueAsString(commentDTO)));

    CloseableHttpResponse httpResponse = client.execute(httpPost);

    return new ResponseWrapper<>(httpResponse, new TypeReference<CommentDTO>() {
    });

  }

  @SneakyThrows
  public ResponseWrapper<CommentDTO> updateComment(CommentDTO commentDTO) {
    HttpPut httpPut = new HttpPut(BASE_PATH + "/api/user/comment/");
    httpPut.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
    httpPut.setEntity(new StringEntity(mapper.writeValueAsString(commentDTO)));

    CloseableHttpResponse httpResponse = client.execute(httpPut);

    return new ResponseWrapper<>(httpResponse, new TypeReference<CommentDTO>() {
    });
  }

  @SneakyThrows
  public ResponseWrapper<List<CommentDTO>> getItemComments(Long commentId) {
    HttpGet httpGet = new HttpGet(BASE_PATH + "/admin/api/v2/shopItem/" + commentId + "/comments");
    CloseableHttpResponse httpResponse = client.execute(httpGet);
    return new ResponseWrapper<>(httpResponse, new TypeReference<List<CommentDTO>>() {
    });
  }
  @SneakyThrows
  public ResponseWrapper<List<CommentDTO>> getAllComments() {
    HttpGet httpGet = new HttpGet(BASE_PATH + "/api/user/comments/");
    CloseableHttpResponse httpResponse = client.execute(httpGet);
    return new ResponseWrapper<>(httpResponse, new TypeReference<List<CommentDTO>>() {
    });
  }
  @SneakyThrows
  public ResponseWrapper<String> deleteComment(CommentDTO commentDTO) {
    HttpDelete httpDelete = new HttpDelete(BASE_PATH + "/api/user/comment"+"/"+commentDTO.getId());
    httpDelete.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));

    CloseableHttpResponse httpResponse = client.execute(httpDelete);
    return new ResponseWrapper<>(httpResponse, new TypeReference<String>() {
    });}

    @SneakyThrows
    public ResponseWrapper<CommentDTO> getComment(Long commentId) {
      HttpGet httpGet = new HttpGet(BASE_PATH + "/api/comment/" + commentId);
      CloseableHttpResponse httpResponse = client.execute(httpGet);
      return new ResponseWrapper<>(httpResponse, new TypeReference<CommentDTO>() {
      });
    }

  @SneakyThrows
  public ResponseWrapper<OrderDTO> createOrderSlow(OrderDTO orderDto) {
    HttpPost httpPost = new HttpPost(BASE_PATH + "/api/user/orderSlow");
    httpPost.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
    httpPost.setEntity(new StringEntity(mapper.writeValueAsString(orderDto)));

    CloseableHttpResponse httpResponse = client.execute(httpPost);

    return new ResponseWrapper<>(httpResponse, new TypeReference<OrderDTO>() {
    });

  }

  @SneakyThrows
  public ResponseWrapper<OrderDTO> createOrderFast(OrderDTO orderDto) {
    HttpPost httpPost = new HttpPost(BASE_PATH + "/api/user/orderFast");
    httpPost.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
    httpPost.setEntity(new StringEntity(mapper.writeValueAsString(orderDto)));

    CloseableHttpResponse httpResponse = client.execute(httpPost);

    return new ResponseWrapper<>(httpResponse, new TypeReference<OrderDTO>() {
    });

  }
  @SneakyThrows
  public ResponseWrapper<OrderDTO> getOrderById(int orderId) {
    HttpGet httpGet = new HttpGet(BASE_PATH + "/api/user/order/"+orderId);
    CloseableHttpResponse httpResponse = client.execute(httpGet);
    return new ResponseWrapper<>(httpResponse, new TypeReference<OrderDTO>() {
    });

  }

}
