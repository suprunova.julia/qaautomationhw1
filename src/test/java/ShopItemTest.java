import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class ShopItemTest {
  @BeforeClass
  public void setUp() {
    RestAssured.baseURI = "http://207.154.241.206:8080";
    RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
  }

  @Test(testName = "1. Create an item without a category (positive)")
  public void createItemWithoutCategory() {
    String body = "{\n" +
        "\"itemDescription\": \"TV\",\n" +
        " \"itemName\": \"LG 12345\",\n" +
        " \"price\": 16,\n" +
        " \"quantity\": 1\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201);
  }

  @Test(testName = "2. Create an item with a category (positive)")
  public void createItemWithCategory() {
    String body = "{\n" +
        " \"categoryName\": \"Smart TV\",\n" +
        "\"categoryDescription\": \"Samsung E556GT\"\n" +
        "   }";
    long id = given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/category")
        .then()
        .statusCode(201)
        .extract().response().jsonPath().getLong("id");

    body = "{\n" +
        " \"categoryId\":" + id + " ,\n" +
        " \"id\": 321,\n" +
        "\"itemDescription\": \"TV\",\n" +
        " \"itemName\": \"LG 12345\",\n" +
        " \"price\": 16,\n" +
        " \"quantity\": 1\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201);
  }

  @Test(testName = "2. Create an item with a category (positive)")
  public void createItemWithNullCategoryId() {
    String body = "{\n" +
        " \"categoryId\": null,\n" +
        " \"id\": 321,\n" +
        "\"itemDescription\": \"TV\",\n" +
        " \"itemName\": \"LG 12345\",\n" +
        " \"price\": 16,\n" +
        " \"quantity\": 1\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201);
  }

  @Test(testName = "2. Create an item with a category (negative)")
  public void createItemWithStringCategoryId() {
    String body = "{\n" +
        " \"categoryId\": \"abc\",\n" +
        " \"id\": 321,\n" +
        "\"itemDescription\": \"TV\",\n" +
        " \"itemName\": \"LG 12345\",\n" +
        " \"price\": 16,\n" +
        " \"quantity\": 1\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(400);
  }

  @Test(testName = "3. Validate itemDescription field requirements (positive)")
  public void validateItemDescriptionField() {
    String body = "{\n" +
        "\"itemDescription\": \"Mixer\",\n" +
        " \"itemName\": \"M 12345\",\n" +
        " \"price\": 16,\n" +
        " \"quantity\": 1\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201);
  }

  @Test(testName = "3. Validate itemDescription field requirements (positive)")
  public void validateItemDescriptionFieldMissing() {
    String body = "{\n" +
        " \"itemName\": \"M 12345\",\n" +
        " \"price\": 16,\n" +
        " \"quantity\": 1\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201);
  }

  @Test(testName = "3. Validate itemDescription field requirements (positive)")
  public void validateItemDescriptionFieldNull() {
    String body = "{\n" +
        "\"itemDescription\": null,\n" +
        " \"itemName\": \"M 12345\",\n" +
        " \"price\": 16,\n" +
        " \"quantity\": 1\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201);
  }

  @Test(testName = "4. Validate itemName field requirements (positive)")
  public void validateItemNameField() {
    String body = "{\n" +
        "\"itemDescription\": \"Cooker\",\n" +
        " \"itemName\": \"C\",\n" +
        " \"price\": 1689,\n" +
        " \"quantity\": 0\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201)
        .body("itemName", notNullValue());
  }

  @Test(testName = "4. Validate itemName field requirements (negative)")
  public void emptyNameField() {
    String body = "{\n" +
        "\"itemDescription\": \"Cooker\",\n" +
        " \"itemName\": \"\",\n" +
        " \"price\": 1689,\n" +
        " \"quantity\": 0\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(400);
  }

  @Test(testName = "4. Validate itemName field requirements (negative)")
  public void nullItemNameField() {
    String body = "{\n" +
        "\"itemDescription\": \"Cooker\",\n" +
        " \"itemName\": null,\n" +
        " \"price\": 1689,\n" +
        " \"quantity\": 0\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(400);
  }

  @Test(testName = "4. Validate itemName field requirements (negative)")
  public void withoutItemNameField() {
    String body = "{\n" +
        "\"itemDescription\": \"Cooker\",\n" +
        " \"price\": 1689,\n" +
        " \"quantity\": 0\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(400);
  }

  @Test(testName = "5. Validate price field requirements (positive)")
  public void validatePriceField() {
    String body = "{\n" +
        "\"itemDescription\": \"Lego wheel\",\n" +
        " \"itemName\": \"wheel\",\n" +
        " \"price\": 1,\n" +
        " \"quantity\": 0\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201);
  }

  @Test(testName = "5. Validate price field requirements (negative)")
  public void zeroPriceField() {
    String body = "{\n" +
        "\"itemDescription\": \"Lego wheel\",\n" +
        " \"itemName\": \"wheel\",\n" +
        " \"price\": 0,\n" +
        " \"quantity\": 0\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(400);
  }

  @Test(testName = "5. Validate price field requirements (negative)")
  public void nullPriceField() {
    String body = "{\n" +
        "\"itemDescription\": \"Lego wheel\",\n" +
        " \"itemName\": \"wheel\",\n" +
        " \"price\": null,\n" +
        " \"quantity\": 0\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(400);
  }

  @Test(testName = "5. Validate price field requirements (negative)")
  public void stringPriceField() {
    String body = "{\n" +
        "\"itemDescription\": \"Lego wheel\",\n" +
        " \"itemName\": \"wheel\",\n" +
        " \"price\": \"45\",\n" +
        " \"quantity\": 0\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(400);
  }


  @Test(testName = "5. Validate price field requirements (negative)")
  public void negativePriceField() {
    String body = "{\n" +
        "\"itemDescription\": \"Lego wheel\",\n" +
        " \"itemName\": \"wheel\",\n" +
        " \"price\": -45,\n" +
        " \"quantity\": 0\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(400);
  }

  @Test(testName = "6. Validate quantity field requirements (positive)")
  public void validateQuantityField() {
    String body = "{\n" +
        "\"itemDescription\": \"Lego wheel\",\n" +
        " \"itemName\": \"wheel\",\n" +
        " \"price\": 1,\n" +
        " \"quantity\": 0\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201);
  }

  @Test(testName = "6. Validate quantity field requirements (positive)")
  public void positiveQuantityField() {
    String body = "{\n" +
        "\"itemDescription\": \"Lego wheel\",\n" +
        " \"itemName\": \"wheel\",\n" +
        " \"price\": 1,\n" +
        " \"quantity\": 1\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201);
  }

  @Test(testName = "6. Validate quantity field requirements (negative)")
  public void nullQuantityField() {
    String body = "{\n" +
        "\"itemDescription\": \"Lego wheel\",\n" +
        " \"itemName\": \"wheel\",\n" +
        " \"price\": 1,\n" +
        " \"quantity\": null\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(400);
  }

  @Test(testName = "6. Validate quantity field requirements (negative)")
  public void stringQuantityField() {
    String body = "{\n" +
        "\"itemDescription\": \"Lego wheel\",\n" +
        " \"itemName\": \"wheel\",\n" +
        " \"price\": 1,\n" +
        " \"quantity\": \"2\"\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(400);
  }

  @Test(testName = "7. Create an item and get it (positive)")
  public void createItemGetIt() {
    String body = "{\n" +
        "\"itemDescription\": \"TV\",\n" +
        " \"itemName\": \"LG 12345\",\n" +
        " \"price\": 16,\n" +
        " \"quantity\": 1\n" +
        "   }";

    long id = RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201)
        .extract().response().jsonPath().getLong("id");

    given()
        .when()
        .get("/admin/api/v1/shopItem/" + id)
        .then()
        .statusCode(200)
        .body("id", equalTo((int) id));
  }

  @Test(testName = "8. Create an item and update it (positive)")
  public void createItemUpdateIt() {
    String body = "{\n" +
        "\"itemDescription\": \"TV\",\n" +
        " \"itemName\": \"LG 12345\",\n" +
        " \"price\": 16,\n" +
        " \"quantity\": 1\n" +
        "   }";

    long id = RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201)
        .extract().response().jsonPath().getLong("id");

    body = "{\n" +
        "\"id\":" + id + ",\n" +
        "\"itemDescription\": \"TV\",\n" +
        " \"itemName\": \"LG 12345\",\n" +
        " \"price\": 60,\n" +
        " \"quantity\": 1\n" +
        "   }";

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .put("/admin/api/v1/shopItem")
        .then()
        .statusCode(200)
        .body("price", equalTo(60F));
  }

  @Test(testName = "9. Create an item and delete it (positive)")
  public void createItemDeleteIt() {
    String body = "{\n" +
        "\"itemDescription\": \"TV\",\n" +
        " \"itemName\": \"LG 12345\",\n" +
        " \"price\": 16,\n" +
        " \"quantity\": 1\n" +
        "   }";

    long id = RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/admin/api/v1/shopItem")
        .then()
        .statusCode(201)
        .extract().response().jsonPath().getLong("id");

    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .body(body)
        .delete("/admin/api/v1/shopItem/" + id)
        .then()
        .statusCode(204);

    given()
        .when()
        .get("/admin/api/v1/shopItem/" + id)
        .then()
        .statusCode(404);
  }

  @Test(testName = "10. Get all items (positive)")
  public void getAllItems() {
    RestAssured
        .given()
        .when()
        .contentType(ContentType.JSON)
        .get("/admin/api/v1/shopItems")
        .then()
        .statusCode(200);
  }

}